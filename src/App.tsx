import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { apolloClient } from './graphql/apolloClient';

import DocumentView from './components/DocumentView';
import ArtboardView from './components/ArtboardView';

function App() {
    return (
        <ApolloProvider client={apolloClient}>
            <Router>
                <Switch>
                    <Route exact path="/doc/:documentId">
                        <DocumentView />
                    </Route>
                    <Route exact path="/doc/:documentId/:artboardId">
                        <ArtboardView />
                    </Route>
                </Switch>
            </Router>
        </ApolloProvider>
    );
}

export default App;
