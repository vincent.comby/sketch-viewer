export const SKETCH_API_URL = 'https://graphql.sketch.cloud/api';

export const TOP_NAV_BAR_HEIGHT = '64px';
