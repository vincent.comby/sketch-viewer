import { useQuery } from '@apollo/client';

import { GET_DOCUMENT_QUERY } from '../graphql/queries';
import { SharedDocument } from '../graphql/types';

interface GetDocumentQueryVars {
    documentId: string;
}

/**
 * Custom hook wrapping useQuery to fetch a document.
 * @param documentId
 */
export function useGetDocumentQuery(documentId: string) {
    return useQuery<SharedDocument, GetDocumentQueryVars>(GET_DOCUMENT_QUERY, {
        variables: { documentId },
    });
}
