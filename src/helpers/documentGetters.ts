import { Artboard, SharedDocument, Document } from '../graphql/types';

/**
 * This module contains some getter functions to help extracting information
 * from the data returned by the GraphQL API in a consistent way.
 */

export function getArtboardThumbnail(artboard: Artboard): string | undefined {
    return artboard.files[0]?.thumbnails[0]?.url;
}

export function getDocumentFromSharedDocument(
    sharedDocument?: SharedDocument
): Document | undefined {
    return sharedDocument?.share.version.document;
}

export function getDocumentArtboardList(document?: Document): Artboard[] {
    return document?.artboards.entries || [];
}

export function getArtboardFromId(
    artboardId: string,
    document?: Document
): Artboard | undefined {
    if (!document) {
        return;
    }
    const artboardList = getDocumentArtboardList(document);

    return artboardList.find((artboardItem) => {
        return artboardItem.id === artboardId;
    });
}

/**
 * Returned index is zero-based.
 */
export function getArtboardIndexInList(
    artboardId: string,
    artboardList: Artboard[]
): number {
    return artboardList.findIndex((artboardItem) => {
        return artboardItem.id === artboardId;
    });
}

/**
 * Extract source URL of artboard image.
 * Since an artboard can contain multiple scales of the same image,
 * we also create a sourceSet attribute if you want
 * to handle devices with pixel ratios higher than 1.
 */
export function getArtboardImagesSources(
    artboard?: Artboard
): { sourceSet: string; source: string } {
    if (!artboard) {
        return {
            sourceSet: '',
            source: '',
        };
    }
    const srcSet = artboard.files.map((file) => `${file.url} ${file.scale}x`);

    return {
        sourceSet: srcSet.join(', '),
        source: artboard.files[0]?.url || '',
    };
}
