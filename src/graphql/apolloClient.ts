import { ApolloClient, InMemoryCache } from '@apollo/client';

import { SKETCH_API_URL } from '../constants';

export const apolloClient = new ApolloClient({
    uri: SKETCH_API_URL,
    cache: new InMemoryCache(),
});
