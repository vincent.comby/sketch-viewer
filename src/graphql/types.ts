/**
 * Types we use to represent the data coming from
 * the GraphQL API.
 */

export interface SharedDocument {
    share: {
        shortId: string;
        version: {
            document: Document;
        };
    };
}

export interface Document {
    artboards: {
        entries: Artboard[];
    };
    name: string;
}

export interface Artboard {
    files: ArtboardFile[];
    name: string;
    id: string;
    width: number;
    height: number;
}

export interface ArtboardFile {
    height: number;
    width: number;
    thumbnails: ArtboardThumbnail[];
    scale: number;
    url: string;
}

export interface ArtboardThumbnail {
    height: number;
    width: number;
    url: string;
}
