import { gql } from '@apollo/client';

export const GET_DOCUMENT_QUERY = gql`
    query getDocument($documentId: ID!) {
        share(shortId: $documentId) {
            shortId
            version {
                document {
                    name
                    artboards {
                        entries {
                            name
                            id
                            isArtboard
                            width
                            height
                            files {
                                url
                                height
                                width
                                scale
                                thumbnails {
                                    url
                                    height
                                    width
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;
