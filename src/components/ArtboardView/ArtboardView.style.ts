import styled from 'styled-components';
import { TOP_NAV_BAR_HEIGHT } from '../../constants';

import ImageWithSpinner from '../common/ImageWithSpinner';

/* ArtboardViewerContainer should take the space of the whole viewport minus the top nav bar */
export const ArtboardViewerContainer = styled.div`
    width: 100vw;
    height: calc(100vh - ${TOP_NAV_BAR_HEIGHT});
    padding: 50px;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
`;

/* Image can be smaller than the ArtboardViewerContainer but never bigger. */
export const ArtboardViewerImage = styled(ImageWithSpinner)`
    max-width: 100%;
    max-height: 100%;
    object-fit: scale-down;
`;
