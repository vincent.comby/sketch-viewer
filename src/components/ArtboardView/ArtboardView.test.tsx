import React from 'react';

import ArtboardView from './index';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { ArtboardViewWrapper } from '../../../test/test-utils';

/**
 * These tests are using mock API data you can find in test/documentQueryMocks.ts
 */
describe('ArtboardView', () => {
    beforeEach(() => {
        window.history.replaceState(
            {},
            'Test page',
            '/doc/fake-id/3b06bde9-e7fb-45f2-a359-79aee8dfeaa5'
        );
    });

    afterEach(cleanup);

    test('It should show a spinner while loading the data', () => {
        const { getByTestId } = render(<ArtboardView />, {
            wrapper: ArtboardViewWrapper,
        });

        expect(getByTestId('page-loader')).toBeTruthy();
    });

    test('It should display the artboard image', async () => {
        const { findByTestId, queryByTestId } = render(<ArtboardView />, {
            wrapper: ArtboardViewWrapper,
        });

        const artboardImage = await findByTestId('artboard-image');

        // Loading spinner should also be gone
        expect(queryByTestId('page-loader')).toBeNull();
        expect(artboardImage).toBeTruthy();
        expect(artboardImage).toHaveAttribute(
            'src',
            'https://resources-live.sketch.cloud/files/2cde8deb-4623-4333-b0e4-c2d5c893d875.png?Expires=1604451600&Signature=Q28voMIzOB3rvoJUPuyNJ0CDQubLhMdnSLsZQXGenRWL~~O4rcxmw1lOhOW3roy3RpgK7qVfUrKxiiEhwBIthaxvD9hVmyEtyyp5DePcnEUxiI3TqBtGSGZrqGZJHgRr97GShL~JQHnBHUoN10kzxU7Cd2N86zLSX2dY3uvyOaI_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA'
        );
    });

    test('It should have the artboard title in the nav bar', async () => {
        const { findByTestId } = render(<ArtboardView />, {
            wrapper: ArtboardViewWrapper,
        });

        expect(await findByTestId('artboard-navbar-content')).toHaveTextContent(
            'iPhone 8'
        );
    });

    test('It should show the current artboard index', async () => {
        const { findByTestId } = render(<ArtboardView />, {
            wrapper: ArtboardViewWrapper,
        });

        expect(await findByTestId('current-artboard-index')).toHaveTextContent(
            /^1$/
        );
    });

    test('It should show the number of artboards', async () => {
        const { findByTestId } = render(<ArtboardView />, {
            wrapper: ArtboardViewWrapper,
        });

        expect(await findByTestId('number-of-artboards')).toHaveTextContent(
            /^2$/
        );
    });

    describe('Artboard nav bar controls', () => {
        const artboard1 = {
            title: 'iPhone 8',
            id: '3b06bde9-e7fb-45f2-a359-79aee8dfeaa5',
            imageSrc:
                'https://resources-live.sketch.cloud/files/2cde8deb-4623-4333-b0e4-c2d5c893d875.png?Expires=1604451600&Signature=Q28voMIzOB3rvoJUPuyNJ0CDQubLhMdnSLsZQXGenRWL~~O4rcxmw1lOhOW3roy3RpgK7qVfUrKxiiEhwBIthaxvD9hVmyEtyyp5DePcnEUxiI3TqBtGSGZrqGZJHgRr97GShL~JQHnBHUoN10kzxU7Cd2N86zLSX2dY3uvyOaI_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
        };
        const artboard2 = {
            title: 'iPhone 8 Copy',
            id: '717e9bc5-85a8-4dfa-9ee1-3748b3756c1e',
            imageSrc:
                'https://resources-live.sketch.cloud/files/6e217d47-d7f3-480e-9246-fe87ce866bcd.png?Expires=1604451600&Signature=UhyaTWtwb7i8vULZ7wtZJWfvq1iR1KwOA2C0i74VjM1HBd6SqO7bOZ4zihDz~uQsLW6IhqF6Lrjs3E~oS6PcbwnPoscOPPORi-S0CgnWVYAXSK~GR1MO48F~4rdxR~rTMkqCCcRmW5S7bjf-zerCO4LLoBYG-dHP4Jx1Mqx6uus_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
        };
        test('Clicking the close icon should redirect to the document view page', async () => {
            const { findByTestId } = render(<ArtboardView />, {
                wrapper: ArtboardViewWrapper,
            });

            fireEvent.click(await findByTestId('close-artboard'));

            expect(window.location.href).toBe('http://localhost/doc/fake-id');
        });

        test('Clicking the left arrow icon should not do anything when artboard is the first one', async () => {
            // First artboard is loaded by default
            const { findByTestId } = render(<ArtboardView />, {
                wrapper: ArtboardViewWrapper,
            });

            fireEvent.click(await findByTestId('arrow-left'));

            // Nothing changes
            expect(window.location.href).toBe(
                `http://localhost/doc/fake-id/${artboard1.id}`
            );
        });

        test('Clicking the left arrow icon should move to the previous artboard when not first one', async () => {
            // Load second artboard
            window.history.replaceState(
                {},
                'Test page',
                `/doc/fake-id/${artboard2.id}`
            );
            const { findByTestId } = render(<ArtboardView />, {
                wrapper: ArtboardViewWrapper,
            });

            fireEvent.click(await findByTestId('arrow-left'));

            // Switched to the first artboard.
            expect(window.location.href).toBe(
                `http://localhost/doc/fake-id/${artboard1.id}`
            );

            expect(
                await findByTestId('artboard-navbar-content')
            ).toHaveTextContent(artboard1.title);

            expect(await findByTestId('artboard-image')).toHaveAttribute(
                'src',
                artboard1.imageSrc
            );
        });

        test('Clicking the right arrow icon should not do anything when artboard is the last one', async () => {
            // Load second artboard (last one)
            window.history.replaceState(
                {},
                'Test page',
                `/doc/fake-id/${artboard2.id}`
            );
            const { findByTestId } = render(<ArtboardView />, {
                wrapper: ArtboardViewWrapper,
            });

            fireEvent.click(await findByTestId('arrow-right'));

            // Nothing changes
            expect(window.location.href).toBe(
                `http://localhost/doc/fake-id/${artboard2.id}`
            );
        });

        test('Clicking the right arrow icon should move to the next artboard when not last one', async () => {
            // First artboard is loaded by default
            const { findByTestId } = render(<ArtboardView />, {
                wrapper: ArtboardViewWrapper,
            });

            fireEvent.click(await findByTestId('arrow-right'));

            // Switched to the second artboard.
            expect(window.location.href).toBe(
                `http://localhost/doc/fake-id/${artboard2.id}`
            );

            expect(
                await findByTestId('artboard-navbar-content')
            ).toHaveTextContent(artboard2.title);

            expect(await findByTestId('artboard-image')).toHaveAttribute(
                'src',
                artboard2.imageSrc
            );
        });
    });
});
