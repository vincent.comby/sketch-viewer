import styled, { css } from 'styled-components';

import { ReactComponent as LeftArrowSvg } from '../../../assets/arrow-left.svg';
import { ReactComponent as RightArrowSvg } from '../../../assets/arrow-right.svg';
import { ReactComponent as SlashSeparatorSvg } from '../../../assets/breadcrumb.svg';

export const NavigationControls = styled.div`
    display: grid;
    grid-template-columns: 10px 80px 10px;
    color: #808080;
    user-select: none;
`;

/** Use "$" prefix to avoid passing the prop to the DOM element
 * https://styled-components.com/docs/api#transient-props */
const arrowCommonCSS = css<{ $isDisabled: boolean }>`
    height: 17px;
    stroke: #808080;
    opacity: ${(props) => (props.$isDisabled ? '0.5' : '1')};

    ${({ $isDisabled }) =>
        !$isDisabled &&
        `
            &:hover {
                stroke: #222222;
            }
        `}
`;

export const LeftArrow = styled(LeftArrowSvg)<{ $isDisabled: boolean }>`
    ${arrowCommonCSS};
`;

export const RightArrow = styled(RightArrowSvg)<{ $isDisabled: boolean }>`
    ${arrowCommonCSS};
`;

export const SlashSeparator = styled(SlashSeparatorSvg)`
    margin: 0 7px;
`;

export const PaginationInfoContainer = styled.div`
    text-align: center;
`;
