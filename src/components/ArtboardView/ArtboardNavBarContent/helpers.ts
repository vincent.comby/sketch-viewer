import { Artboard } from '../../../graphql/types';

/**
 * From a list of artboards, return, in the shape of a tuple, the artboard id of
 * the item before and after the current artboard index.
 * The previous and next items might be undefined when the current index
 * value is one of the edge of the array (0 or length - 1).
 * The currentArtboardIndex is expected to be Zero-based.
 */
export function getPrevAndNextArtboardIds(
    artboardList: Artboard[],
    currentArtboardIndex: number
): [string | undefined, string | undefined] {
    const hasPrevious = currentArtboardIndex - 1 >= 0;
    const hasNext = currentArtboardIndex + 1 < artboardList.length;

    let previousArtboardId;
    if (hasPrevious) {
        previousArtboardId = artboardList[currentArtboardIndex - 1].id;
    }
    let nextArtboardId;
    if (hasNext) {
        nextArtboardId = artboardList[currentArtboardIndex + 1].id;
    }

    return [previousArtboardId, nextArtboardId];
}
