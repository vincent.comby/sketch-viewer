import React from 'react';
import { Link } from 'react-router-dom';

import { Artboard } from '../../../graphql/types';

import NavigationControls from './NavigationControls';
import * as Styled from './ArtboardNavBarContent.style';

type ArtboardNavBarContentProps = {
    documentId?: string;
    currentArtboard?: Artboard;
    artboardList?: Artboard[];
};

export default function ArtboardNavBarContent({
    documentId,
    currentArtboard,
    artboardList,
}: ArtboardNavBarContentProps) {
    return (
        <Styled.ArtboardNavBarContent data-testid="artboard-navbar-content">
            <Styled.ControlsContainer>
                <Link to={`/doc/${documentId}`}>
                    <Styled.CloseIcon data-testid="close-artboard" />
                </Link>
                <Styled.PipeSeparator />
                <NavigationControls
                    documentId={documentId}
                    currentArtboard={currentArtboard}
                    artboardList={artboardList}
                />
            </Styled.ControlsContainer>
            <Styled.ArtboardTitle>{currentArtboard?.name}</Styled.ArtboardTitle>
        </Styled.ArtboardNavBarContent>
    );
}
