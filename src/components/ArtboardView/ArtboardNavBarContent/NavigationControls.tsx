import React from 'react';
import { Link } from 'react-router-dom';

import { Artboard } from '../../../graphql/types';
import { getArtboardIndexInList } from '../../../helpers/documentGetters';
import { getPrevAndNextArtboardIds } from './helpers';

import * as Styled from './NavigationControls.style';

type NavigationControlsProps = {
    documentId?: string;
    currentArtboard?: Artboard;
    artboardList?: Artboard[];
};

/**
 * Arrow controls + pagination info to navigate to the prev/next artboards.
 */
export default function NavigationControls({
    documentId,
    currentArtboard,
    artboardList,
}: NavigationControlsProps) {
    // If artboard not yet loaded or something else went wrong.
    if (!documentId || !currentArtboard || !artboardList) {
        return null;
    }

    const currentIndex = getArtboardIndexInList(
        currentArtboard.id,
        artboardList
    );

    if (currentIndex < 0) {
        // This should never happen, unless we have corrupted data.
        throw new Error('Something went wrong.');
    }

    const [previousArtboardId, nextArtboardId] = getPrevAndNextArtboardIds(
        artboardList,
        currentIndex
    );

    const previousArtboardURL = previousArtboardId
        ? `/doc/${documentId}/${previousArtboardId}`
        : undefined;
    const nextArtboardURL = nextArtboardId
        ? `/doc/${documentId}/${nextArtboardId}`
        : undefined;

    return (
        <Styled.NavigationControls>
            <NavigationArrow
                dataTestId="arrow-left"
                toURL={previousArtboardURL}
                direction="left"
            />
            <Styled.PaginationInfoContainer>
                <span data-testid="current-artboard-index">
                    {currentIndex + 1}
                </span>
                <Styled.SlashSeparator />
                <span data-testid="number-of-artboards">
                    {artboardList.length}
                </span>
            </Styled.PaginationInfoContainer>
            <NavigationArrow
                dataTestId="arrow-right"
                toURL={nextArtboardURL}
                direction="right"
            />
        </Styled.NavigationControls>
    );
}

type NavigationArrowProps = {
    toURL?: string;
    direction: 'left' | 'right';
    dataTestId: string;
};
/**
 * If a URL is provided, wrap the arrow into a Link,
 * otherwise just return the arrow.
 * This is to handle the scenario where an arrow is disabled,
 * in that scenario we don't want to wrap in a Link.
 */
function NavigationArrow({
    toURL,
    direction,
    dataTestId,
}: NavigationArrowProps) {
    const isDisabled = !toURL;
    const ArrowComponent =
        direction === 'left' ? Styled.LeftArrow : Styled.RightArrow;

    if (!toURL) {
        return (
            // Wrap arrow in span to compensate for the line-height
            // of the non-existing <Link> wrapper element.
            <span>
                <ArrowComponent
                    data-testid={dataTestId}
                    $isDisabled={isDisabled}
                />
            </span>
        );
    }

    return (
        <Link to={toURL}>
            <ArrowComponent data-testid={dataTestId} $isDisabled={isDisabled} />
        </Link>
    );
}
