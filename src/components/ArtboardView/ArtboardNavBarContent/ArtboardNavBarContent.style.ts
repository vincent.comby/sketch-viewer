import styled from 'styled-components';

import { ReactComponent as CloseSvg } from '../../../assets/close.svg';
import { ReactComponent as PipeSeparatorSvg } from '../../../assets/separator.svg';

/**
 * Split the content in 3 equal columns even though the last one will be empty.
 * The goal is just to keep the ArtboardTitle (2nd column) in the center.
 */
export const ArtboardNavBarContent = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
`;

export const CloseIcon = styled(CloseSvg)`
    width: 12px;
    height: 12px;
    stroke: #808080;
    &:hover {
        stroke: #222222;
    }
`;

export const PipeSeparator = styled(PipeSeparatorSvg)`
    height: 32px;
    margin: 0 16px;
`;

export const ArtboardTitle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
    font-weight: 600;
`;

export const ControlsContainer = styled.div`
    display: flex;
    align-items: center;
`;
