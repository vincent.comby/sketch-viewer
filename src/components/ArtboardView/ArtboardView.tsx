import React from 'react';
import { useParams } from 'react-router-dom';

import { useGetDocumentQuery } from '../../hooks/useGetDocumentQuery';
import {
    getArtboardFromId,
    getDocumentFromSharedDocument,
    getDocumentArtboardList,
    getArtboardImagesSources,
} from '../../helpers/documentGetters';
import Page from '../common/Page';

import ArtboardNavBarContent from './ArtboardNavBarContent';
import * as Styled from './ArtboardView.style';

type ArtboardViewRouteParams = {
    documentId: string;
    artboardId: string;
};

export default function ArtboardView() {
    const { documentId, artboardId } = useParams<ArtboardViewRouteParams>();

    const { loading, error, data: sharedDocument } = useGetDocumentQuery(
        documentId
    );
    let errorMessage = error?.message;
    const document = getDocumentFromSharedDocument(sharedDocument);
    const artboard = getArtboardFromId(artboardId, document);
    const artboardList = getDocumentArtboardList(document);

    const isArtboardMissing = !loading && !artboard;

    // We don't want to overwrite a potential API error!
    if (!error && isArtboardMissing) {
        errorMessage = "Sorry, this artboard doesn't seem to exist";
    }

    const imageSources = getArtboardImagesSources(artboard);

    return (
        <Page
            navBarContent={
                <ArtboardNavBarContent
                    currentArtboard={artboard}
                    documentId={documentId}
                    artboardList={artboardList}
                />
            }
            isLoading={loading}
            error={errorMessage}
        >
            {artboard && (
                <Styled.ArtboardViewerContainer>
                    <Styled.ArtboardViewerImage
                        /* Use key attribute to make sure react mount/unmount
                         * images between two artboards and avoid flickering
                         * effect when only switching the src attribute. */
                        key={imageSources.source}
                        width={artboard.width}
                        height={artboard.height}
                        src={imageSources.source}
                        srcSet={imageSources.sourceSet}
                        data-testid="artboard-image"
                    />
                </Styled.ArtboardViewerContainer>
            )}
        </Page>
    );
}
