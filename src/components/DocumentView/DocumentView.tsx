import React from 'react';
import { useParams } from 'react-router-dom';

import {
    getDocumentFromSharedDocument,
    getDocumentArtboardList,
} from '../../helpers/documentGetters';
import { useGetDocumentQuery } from '../../hooks/useGetDocumentQuery';

import Page from '../common/Page';
import DocumentNavBarContent from './DocumentNavBarContent';
import ArtboardPreviewCard from './ArtboardPreviewCard';
import * as Styled from './DocumentView.style';

type DocumentViewRouteParams = {
    documentId: string;
};

export default function DocumentView() {
    const { documentId } = useParams<DocumentViewRouteParams>();

    const { loading, error, data: sharedDocument } = useGetDocumentQuery(
        documentId
    );

    const document = getDocumentFromSharedDocument(sharedDocument);
    const artboardList = getDocumentArtboardList(document);

    return (
        <Page
            navBarContent={
                <DocumentNavBarContent documentName={document?.name} />
            }
            isLoading={loading}
            error={error?.message}
        >
            <Styled.ArtboardList>
                {artboardList.map((artboard) => {
                    return (
                        <Styled.ArtboardListItem key={artboard.id}>
                            <ArtboardPreviewCard
                                documentId={documentId}
                                artboard={artboard}
                            />
                        </Styled.ArtboardListItem>
                    );
                })}
            </Styled.ArtboardList>
        </Page>
    );
}
