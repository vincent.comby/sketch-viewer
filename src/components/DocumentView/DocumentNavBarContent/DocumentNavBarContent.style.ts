import styled from 'styled-components';

import { ReactComponent as SketchLogoSvg } from '../../../assets/sketch-logo.svg';
import { ReactComponent as PipeSeparatorSvg } from '../../../assets/separator.svg';

export const DocumentNavBarContent = styled.div`
    display: flex;
    align-items: center;
`;

export const SketchLogo = styled(SketchLogoSvg)`
    width: 27px;
    height: 27px;
`;

export const PipeSeparator = styled(PipeSeparatorSvg)`
    height: 32px;
    margin: 0 16px;
`;

export const DocumentTitle = styled.div`
    font-family: Helvetica, Arial, sans-serif;
`;
