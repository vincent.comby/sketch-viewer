import React from 'react';

import * as Styled from './DocumentNavBarContent.style';

type DocumentNavBarContentProps = {
    documentName?: string;
};

export default function DocumentNavBarContent({
    documentName,
}: DocumentNavBarContentProps) {
    return (
        <Styled.DocumentNavBarContent data-testid="document-nav-bar-content">
            <Styled.SketchLogo />
            <Styled.PipeSeparator />
            <Styled.DocumentTitle> {documentName} </Styled.DocumentTitle>
        </Styled.DocumentNavBarContent>
    );
}
