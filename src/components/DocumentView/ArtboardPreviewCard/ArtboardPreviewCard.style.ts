import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ImageWithSpinner from '../../common/ImageWithSpinner';

export const ThumbnailContainer = styled.div`
    width: 100%;
    height: 304px;
    font-size: 14px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const ThumbnailImage = styled(ImageWithSpinner)`
    object-fit: contain;
    width: 100%;
    max-height: 100%;
`;

export const ArtboardName = styled.div`
    text-align: center;
    margin-top: 16px;
    height: 20px;
    color: rgba(0, 0, 0, 0.65);
    text-decoration: none;
`;

export const ReactRouterLink = styled(Link)`
    text-decoration: none;
    color: inherit;
`;
