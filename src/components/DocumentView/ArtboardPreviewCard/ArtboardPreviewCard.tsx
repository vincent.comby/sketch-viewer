import React from 'react';

import { getArtboardThumbnail } from '../../../helpers/documentGetters';
import { Artboard } from '../../../graphql/types';

import * as Styled from './ArtboardPreviewCard.style';

type ArtboardPreviewCardProps = {
    documentId: string;
    artboard: Artboard;
};
export default function ArtboardPreviewCard({
    artboard,
    documentId,
}: ArtboardPreviewCardProps) {
    const thumbnailUrl = getArtboardThumbnail(artboard);
    const artboardURL = `/doc/${documentId}/${artboard.id}`;

    return (
        <Styled.ReactRouterLink to={artboardURL}>
            <Styled.ThumbnailContainer data-testid="preview-card">
                <Styled.ThumbnailImage
                    src={thumbnailUrl}
                    alt={`${artboard.name} thumbnail`}
                />
            </Styled.ThumbnailContainer>
            <Styled.ArtboardName>{artboard.name}</Styled.ArtboardName>
        </Styled.ReactRouterLink>
    );
}
