import styled from 'styled-components';

export const ArtboardList = styled.ul`
    padding: 0;
    margin: 0;
    display: flex;
    flex-wrap: wrap;
    list-style-type: none;
`;

export const ArtboardListItem = styled.li`
    min-width: 240px;
    flex-basis: 20%;
    flex-grow: 1;
    margin: 24px 24px 0 24px;
    cursor: pointer;
    &:hover {
        background: #eaeaea;
        border-radius: 5px;
    }
`;
