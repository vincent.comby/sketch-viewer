import React from 'react';

import DocumentView from './index';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { DocumentViewWrapper } from '../../../test/test-utils';

/**
 * These tests are using mock API data you can find in test/documentQueryMocks.ts
 */
describe('DocumentView', () => {
    beforeEach(() => {
        window.history.replaceState({}, 'Test page', '/doc/fake-id');
    });

    afterEach(cleanup);

    test('It should show a spinner while loading the data', () => {
        const { getByTestId } = render(<DocumentView />, {
            wrapper: DocumentViewWrapper,
        });

        expect(getByTestId('page-loader')).toBeTruthy();
    });

    test('It should render the list of preview card', async () => {
        const { findAllByTestId, queryByTestId } = render(<DocumentView />, {
            wrapper: DocumentViewWrapper,
        });

        const allPreviewCards = await findAllByTestId('preview-card');
        expect(allPreviewCards).toHaveLength(2);

        // Loading spinner should also be gone
        expect(queryByTestId('page-loader')).toBeNull();
    });

    test('It should have the document title in the nav bar', async () => {
        const { findByTestId } = render(<DocumentView />, {
            wrapper: DocumentViewWrapper,
        });

        expect(
            await findByTestId('document-nav-bar-content')
        ).toHaveTextContent('Sketch Document');
    });

    test('Clicking the preview card should redirect to the artboard page', async () => {
        const { findByText } = render(<DocumentView />, {
            wrapper: DocumentViewWrapper,
        });

        fireEvent.click(await findByText('iPhone 8'));

        expect(window.location.href).toBe(
            'http://localhost/doc/fake-id/3b06bde9-e7fb-45f2-a359-79aee8dfeaa5'
        );
    });
});
