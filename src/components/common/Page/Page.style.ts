import styled from 'styled-components';

import { TOP_NAV_BAR_HEIGHT } from '../../../constants';

export const TopNavBar = styled.nav`
    background: white;
    position: fixed;
    height: ${TOP_NAV_BAR_HEIGHT};
    top: 0;
    width: 100%;
    padding: 18px 16px;
    box-sizing: border-box;
    border-bottom: 1px solid rgba(0, 0, 0, 0.08);
    box-shadow: rgba(0, 0, 0, 0.05) 0px 2px 5px 0px;
`;

export const Page = styled.div``;

export const PageContent = styled.div`
    padding-top: ${TOP_NAV_BAR_HEIGHT};
`;

export const LoadingContainer = styled.div`
    display: flex;
    justify-content: center;
    padding: 100px 0;
`;

export const ErrorWarningContainer = styled.div`
    text-align: center;
    padding: 100px 0;
    font-size: 20px;
`;
