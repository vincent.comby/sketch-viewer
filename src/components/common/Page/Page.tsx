import React from 'react';
import * as Styled from './Page.style';
import Loader from 'react-loader-spinner';

type PageProps = {
    /**
     * Customize the content of the top nav bar.
     */
    navBarContent: React.ReactNode;
    isLoading: boolean;
    error?: string;
};

const PageView: React.FC<PageProps> = ({
    children,
    navBarContent,
    isLoading,
    error,
}) => {
    let pageContent = children;

    if (isLoading) {
        pageContent = (
            <Styled.LoadingContainer data-testid="page-loader">
                <Loader
                    type="ThreeDots"
                    color="#F2B43E"
                    height={80}
                    width={80}
                />
            </Styled.LoadingContainer>
        );
    }

    if (error) {
        pageContent = (
            <Styled.ErrorWarningContainer>
                Error: {error}
            </Styled.ErrorWarningContainer>
        );
    }

    return (
        <React.Fragment>
            <Styled.TopNavBar> {navBarContent} </Styled.TopNavBar>
            <Styled.PageContent>{pageContent}</Styled.PageContent>
        </React.Fragment>
    );
};

export default PageView;
