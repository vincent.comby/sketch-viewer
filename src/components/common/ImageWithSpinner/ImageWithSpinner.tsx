import React, { useState } from 'react';
import Loader from 'react-loader-spinner';

import * as Styled from './ImageWithSpinner.style';

/**
 * Simple wrapper component to display a loading indicator until the image is fully loaded.
 * Once loaded, it will display a single <img> element.
 */
const ImageWithSpinner: React.FC<React.ImgHTMLAttributes<HTMLImageElement>> = (
    props
) => {
    const [isLoading, setIsLoading] = useState(true);

    return (
        <React.Fragment>
            {/* eslint-disable-next-line jsx-a11y/alt-text */}
            <Styled.Image
                isLoading={isLoading}
                {...props}
                onLoad={() => setIsLoading(false)}
                onError={() => setIsLoading(false)}
            />
            {isLoading && (
                <Styled.ImagePlaceholderContainer>
                    <Loader
                        type="TailSpin"
                        color="#F2B43E"
                        height={50}
                        width={50}
                    />
                </Styled.ImagePlaceholderContainer>
            )}
        </React.Fragment>
    );
};

export default ImageWithSpinner;
