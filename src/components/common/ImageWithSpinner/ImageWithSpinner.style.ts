import styled from 'styled-components';

export const ImagePlaceholderContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export type ImageProps = {
    isLoading: boolean;
};

/**
 * Hide image until the image resource has finished loading.
 */
export const Image = styled.img<ImageProps>`
    ${(props) => props.isLoading && `display: none;`}
`;
