import { GET_DOCUMENT_QUERY } from '../src/graphql/queries';

/** Apollo API requests mocks for our tests */
const mocks = [
    {
        request: {
            query: GET_DOCUMENT_QUERY,
            variables: {
                documentId: 'fake-id',
            },
        },
        result: {
            data: {
                share: {
                    shortId: '40432a93-5434-4059-87b9-545fd1ad6ee0',
                    version: {
                        document: {
                            name: 'Sketch Document',
                            artboards: {
                                entries: [
                                    {
                                        name: 'iPhone 8',
                                        id:
                                            '3b06bde9-e7fb-45f2-a359-79aee8dfeaa5',
                                        isArtboard: true,
                                        width: 375,
                                        height: 667,
                                        files: [
                                            {
                                                url:
                                                    'https://resources-live.sketch.cloud/files/2cde8deb-4623-4333-b0e4-c2d5c893d875.png?Expires=1604451600&Signature=Q28voMIzOB3rvoJUPuyNJ0CDQubLhMdnSLsZQXGenRWL~~O4rcxmw1lOhOW3roy3RpgK7qVfUrKxiiEhwBIthaxvD9hVmyEtyyp5DePcnEUxiI3TqBtGSGZrqGZJHgRr97GShL~JQHnBHUoN10kzxU7Cd2N86zLSX2dY3uvyOaI_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                height: 667,
                                                width: 375,
                                                scale: 1,
                                                thumbnails: [
                                                    {
                                                        url:
                                                            'https://resources-live.sketch.cloud/files/2cde8deb-4623-4333-b0e4-c2d5c893d875.m.png?Expires=1604451600&Signature=qyFMTO1lqxOvBUU49Tty3Np8Fxd6NUQS-nqA3JCpgV1arcAeHycPR5isFgQY~-G~1FHZOZIf-O5-05nyGnT2pI~aYrcS5DJX0YpAO7zKiSL0S03DBXUvOIJ-GGISZok8YxitFP14UA0xEcE8Lp-gez4IcE--SeSTVwsA3fQtmSk_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                        height: 445,
                                                        width: 250,
                                                    },
                                                ],
                                            },
                                            {
                                                url:
                                                    'https://resources-live.sketch.cloud/files/1cf150e5-2065-4caa-8f91-5392b9486f40.png?Expires=1604451600&Signature=J9Y8H2cfYHBZoVucmw9-oDxo4CcgSiLnufIgEFzYGdDcWbzWDuPPNy32-JKNaFGOtrsh~SCFcCTou1rqJGELki4EmyqWZ-xxpJfrVKEUN8DSWHtA~S6WPmGaXgn-fMbLTJlwQ~pYAePdnPNNoFHluJbZgynoS3qpXY~rv4BoACo_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                height: 1334,
                                                width: 750,
                                                scale: 2,
                                                thumbnails: [
                                                    {
                                                        url:
                                                            'https://resources-live.sketch.cloud/files/1cf150e5-2065-4caa-8f91-5392b9486f40.png?Expires=1604451600&Signature=J9Y8H2cfYHBZoVucmw9-oDxo4CcgSiLnufIgEFzYGdDcWbzWDuPPNy32-JKNaFGOtrsh~SCFcCTou1rqJGELki4EmyqWZ-xxpJfrVKEUN8DSWHtA~S6WPmGaXgn-fMbLTJlwQ~pYAePdnPNNoFHluJbZgynoS3qpXY~rv4BoACo_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                        height: 1334,
                                                        width: 750,
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        name: 'iPhone 8 Copy',
                                        id:
                                            '717e9bc5-85a8-4dfa-9ee1-3748b3756c1e',
                                        isArtboard: true,
                                        width: 375,
                                        height: 667,
                                        files: [
                                            {
                                                url:
                                                    'https://resources-live.sketch.cloud/files/6e217d47-d7f3-480e-9246-fe87ce866bcd.png?Expires=1604451600&Signature=UhyaTWtwb7i8vULZ7wtZJWfvq1iR1KwOA2C0i74VjM1HBd6SqO7bOZ4zihDz~uQsLW6IhqF6Lrjs3E~oS6PcbwnPoscOPPORi-S0CgnWVYAXSK~GR1MO48F~4rdxR~rTMkqCCcRmW5S7bjf-zerCO4LLoBYG-dHP4Jx1Mqx6uus_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                height: 667,
                                                width: 375,
                                                scale: 1,
                                                thumbnails: [
                                                    {
                                                        __typename: 'Thumbnail',
                                                        url:
                                                            'https://resources-live.sketch.cloud/files/6e217d47-d7f3-480e-9246-fe87ce866bcd.m.png?Expires=1604451600&Signature=AYxKAcl7aCHiZLDtrU3cFNb4o6tM7Syh5CT~1ZUPRudL7ns6R-voQ-dx2-sA~OR80LwE8c0iUYKk2UCMcANO4nV~xw61R5Z~g3DxkCZZc0BUsZ13K8hGz9LyWzmAGEBY8hGI9nONB2umWiv-cW2UASwGPeVukV6TmCCHeJGHja8_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                        height: 445,
                                                        width: 250,
                                                    },
                                                ],
                                            },
                                            {
                                                url:
                                                    'https://resources-live.sketch.cloud/files/70875b6c-4b45-4c33-9ad7-34f6a4b7295d.png?Expires=1604451600&Signature=TG2OZ9c4KCw~NhBewZVL-YK55qtN8X~WKs8TyBADl1uRDrtsoi7R7fMs-y-jYxgmQXNKs~if31vkncT4QENK-adSsorm693zRJGSJjq6jHmGHnZG5-AqSOJ5WAbAO8HiualXfZbunu2Udqr1O7fZ~A4AeUNRS6TV6yoIRdg8an8_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                height: 1334,
                                                width: 750,
                                                scale: 2,
                                                thumbnails: [
                                                    {
                                                        url:
                                                            'https://resources-live.sketch.cloud/files/70875b6c-4b45-4c33-9ad7-34f6a4b7295d.png?Expires=1604451600&Signature=TG2OZ9c4KCw~NhBewZVL-YK55qtN8X~WKs8TyBADl1uRDrtsoi7R7fMs-y-jYxgmQXNKs~if31vkncT4QENK-adSsorm693zRJGSJjq6jHmGHnZG5-AqSOJ5WAbAO8HiualXfZbunu2Udqr1O7fZ~A4AeUNRS6TV6yoIRdg8an8_&Key-Pair-Id=APKAJOITMW3RWOLNNPYA',
                                                        height: 1334,
                                                        width: 750,
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        },
                    },
                },
            },
        },
    },
];

export default mocks;
