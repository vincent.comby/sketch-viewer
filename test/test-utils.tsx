import React from 'react';

import { BrowserRouter, Route } from 'react-router-dom';

import {
    MockedProvider as ApolloMockedProvider,
    MockedResponse,
} from '@apollo/client/testing';

import documentQueryMocks from './documentQueryMocks';

type DocumentViewWrapperProps = {
    mocks?: MockedResponse[];
};
export const DocumentViewWrapper: React.FC<DocumentViewWrapperProps> = ({
    children,
    mocks,
}) => {
    return (
        <ApolloMockedProvider mocks={mocks} addTypename={false}>
            <BrowserRouter>
                <Route path="/doc/:documentId">{children}</Route>
            </BrowserRouter>
        </ApolloMockedProvider>
    );
};

DocumentViewWrapper.defaultProps = {
    mocks: documentQueryMocks,
};

type ArtboardViewWrapperProps = {
    mocks?: MockedResponse[];
};
export const ArtboardViewWrapper: React.FC<ArtboardViewWrapperProps> = ({
    children,
    mocks,
}) => {
    return (
        <ApolloMockedProvider mocks={mocks} addTypename={false}>
            <BrowserRouter>
                <Route path="/doc/:documentId/:artboardId">{children}</Route>
            </BrowserRouter>
        </ApolloMockedProvider>
    );
};

ArtboardViewWrapper.defaultProps = {
    mocks: documentQueryMocks,
};
