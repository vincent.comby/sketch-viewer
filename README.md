# Sketch viewer

Simple online viewer for Sketch documents & artboards.

## Getting started

### Run app locally

1) Clone the repo
2) Run `yarn install`
3) Run `yarn start`
4) Server should start on port `3000` by default.
5) Go to `http://localhost:3000/doc/{documentId}`

Examples of URLs with data: http://localhost:3000/doc/Y8wDM or http://localhost:3000/doc/4W43q

### Run the tests

Run all: `yarn test --watchAll=false`

Run in watch mode: `yarn test`

## Notes

### General
- The solution has been bootstrapped using create-react-app.
- I have used prettier VScode extension to format the code.
- I had to edit some SVG elements to remove some hardcoded attributes so I can control them through CSS instead.
- I had to add the artboard `width` & `height` props to the provided graphql query as I decided to handle images for different device-ratios (image scale).
- I have aimed to support modern browsers, IE11 is not supported.
- I tried to keep reusability in mind without over engineering it as this is a very small application.

### Project architecture & conventions

I believe project architecture and & code conventions should be decided as a team. In this case, as I am the only contributor, I have picked a project architecture and some conventions that I usually follow for projects of this size. I would be more than happy to discuss them with you if you are curious about some of them.

### Things that I haven't focused on

There are multiples things that I did not focus on as I assumed that they were outside of the scope. In a normal project, I would have probably spent some time on these.

- Pixel perfect design: I tried to come as close a possible without overthinking it, you will probably find few inconsistencies (starting with the fonts...).
- Accessibility.
- Page speed / bundle size.
- Creating some kind of design system. I have hardcoded all the CSS colors for example.
- Advanced error handling.
- Building a home page.

### Bonus features
- Responsive app.
- Some high level tests using react-testing-library.
- Access different document/artboards through the URL.
- Load the artboard image with the best resolution if your device has a pixel ratio higher than 1.
- Show loading spinner while the actual image file is fetching.


**I hope you have a nice, smooth and easy code review :)**